variable "prefix" {
  type = string
  description = "The prefix used for all resources in this example"
  default = "codilime-task"
}

variable "location" {
  type = string
  description = "The Azure location where all resources in this example should be created"
  default = "westeurope"
}

variable "sku" {
  type = string
  description = "The Azure web app SKU tier"
  default = "S1"
}

variable "OS" {
  type = string
  description = "The Azure web app OS"
  default = "Linux"
}