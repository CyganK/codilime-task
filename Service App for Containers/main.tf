provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "codilime-task" {
  name     = "${var.prefix}-resources"
  location = var.location
}

resource "azurerm_service_plan" "codilime-task-service-plan" {
  name                = "${var.prefix}-sp"
  location            = azurerm_resource_group.codilime-task.location
  resource_group_name = azurerm_resource_group.codilime-task.name
  os_type             = "${var.OS}"
  sku_name            = "${var.sku}"
}


resource "azurerm_linux_web_app" "codilime-task-web-app" {
  name                = "${var.prefix}-web-app"
  location            = azurerm_resource_group.codilime-task.location
  resource_group_name = azurerm_resource_group.codilime-task.name
  service_plan_id     = azurerm_service_plan.codilime-task-service-plan.id

  app_settings = {
    "WEBSITES_ENABLE_APP_SERVICE_STORAGE" = "false"
  }

  site_config {
    application_stack {
      docker_image     = "nginxdemos/hello"
      docker_image_tag = "latest"
    }
  }
}

resource "azurerm_virtual_network" "codilime-task-vnet" {
  name                = "${var.prefix}-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.codilime-task.location
  resource_group_name = azurerm_resource_group.codilime-task.name
}

resource "azurerm_subnet" "codilime-task-subnet" {
  name                 = "${var.prefix}-subnet"
  resource_group_name  = azurerm_resource_group.codilime-task.name
  virtual_network_name = azurerm_virtual_network.codilime-task-vnet.name
  address_prefixes     = ["10.0.1.0/24"]

  delegation {
    name = "${var.prefix}-delegation"

    service_delegation {
      name    = "Microsoft.Web/serverFarms"
      actions = ["Microsoft.Network/virtualNetworks/subnets/action"]
    }
  }
}


resource "azurerm_app_service_virtual_network_swift_connection" "codilime-task-vnet-azurerm_app_service_virtual_network_swift_connection" {
  app_service_id = azurerm_linux_web_app.codilime-task-web-app.id
  subnet_id      = azurerm_subnet.codilime-task-subnet.id
}
