from diagrams import Diagram, Cluster
from diagrams.azure.compute import ContainerInstances 
from diagrams.azure.network import VirtualNetworks
from diagrams.azure.network import LoadBalancers
from diagrams.azure.network import PublicIpAddresses
from diagrams.azure.compute import AppServices

with Diagram ("CodiLime Task ACI", show=False):
    with Cluster("Virtual Network"):
        lb = LoadBalancers("Load Balancer")
        ACI = ContainerInstances("Docker app")

    ip = PublicIpAddresses("public IP")

    ip >> lb >> ACI 
with Diagram("CodiLime Task Service App for Containers", show=False):

    lb1 = LoadBalancers("built-in Load Balancer")
    SA = AppServices("Web app")
    vnet = VirtualNetworks("vnet")

    ip = PublicIpAddresses("public IP")

    ip >> lb1 >> SA >> vnet