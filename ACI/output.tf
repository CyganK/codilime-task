output "public-ip" {
  value = azurerm_public_ip.codilime-task-public-ip.ip_address
}