provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "codilime-task" {
  name     = "${var.prefix}-resources"
  location = var.location
}

resource "azurerm_virtual_network" "codilime-task-vnet" {
  name                = "${var.prefix}-vnet"
  location            = azurerm_resource_group.codilime-task.location
  resource_group_name = azurerm_resource_group.codilime-task.name
  address_space       = ["10.1.0.0/16"]
}

resource "azurerm_subnet" "codilime-task-subnet" {
  name                 = "${var.prefix}-subnet"
  resource_group_name  = azurerm_resource_group.codilime-task.name
  virtual_network_name = azurerm_virtual_network.codilime-task-vnet.name
  address_prefixes     = ["10.1.0.0/24"]

  delegation {
    name = "delegation"

    service_delegation {
      name    = "Microsoft.ContainerInstance/containerGroups"
      actions = ["Microsoft.Network/virtualNetworks/subnets/action"]
    }
  }
}

resource "azurerm_network_profile" "codilime-task-network-profile" {
  name                = "${var.prefix}-network-profile"
  location            = azurerm_resource_group.codilime-task.location
  resource_group_name = azurerm_resource_group.codilime-task.name

  container_network_interface {
    name = "codilime-task-network-interface"

    ip_configuration {
      name      = "codilime-task-ip-config"
      subnet_id = azurerm_subnet.codilime-task-subnet.id
    }
  }
}

resource "azurerm_container_group" "codilime-task-container-instance" {
  name                = "${var.prefix}-contaienr-instance"
  location            = azurerm_resource_group.codilime-task.location
  resource_group_name = azurerm_resource_group.codilime-task.name
  ip_address_type     = "Private"
  network_profile_id  = azurerm_network_profile.codilime-task-network-profile.id
  os_type             = "Linux"
  restart_policy      = "Never"

  container {
    name   = "${var.prefix}-nginx-test-app-1"
    image  = "nginxdemos/hello"
    cpu    = "${var.CPU}"
    memory = "${var.memory}"

    ports {
      port     = 80
      protocol = "TCP"
    }
  }
}

resource "azurerm_public_ip" "codilime-task-public-ip" {
  name                = "${var.prefix}-public-ip"
  location            = azurerm_resource_group.codilime-task.location
  resource_group_name = azurerm_resource_group.codilime-task.name
  allocation_method   = "Static"
  sku = "${var.sku}"
}

resource "azurerm_lb" "codilime-task-lb" {
  name                = "${var.prefix}-lb"
  location            = azurerm_resource_group.codilime-task.location
  resource_group_name = azurerm_resource_group.codilime-task.name
  sku = "${var.sku}"

  frontend_ip_configuration {
    name                 = "${var.prefix}-public-ip"
    public_ip_address_id = azurerm_public_ip.codilime-task-public-ip.id
  }
}

resource "azurerm_lb_backend_address_pool" "codilime-task-backend" {
  name            = "${var.prefix}-backend"
  loadbalancer_id = azurerm_lb.codilime-task-lb.id

}

resource "azurerm_lb_backend_address_pool_address" "codilime-task-backend-adress" {
  name            = "${var.prefix}-backend-address-pool"
  backend_address_pool_id = azurerm_lb_backend_address_pool.codilime-task-backend.id
  virtual_network_id = azurerm_virtual_network.codilime-task-vnet.id
  ip_address = azurerm_container_group.codilime-task-container-instance.ip_address
}

resource "azurerm_lb_rule" "codilime-task-lb-rule" {
  loadbalancer_id                = azurerm_lb.codilime-task-lb.id
  name                           = "${var.prefix}-HTTPS-rule"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "${var.prefix}-public-ip"
  enable_tcp_reset = true
  backend_address_pool_ids = [azurerm_lb_backend_address_pool.codilime-task-backend.id]
  probe_id = azurerm_lb_probe.codilime-task-lb-probe.id
}

resource "azurerm_lb_probe" "codilime-task-lb-probe" {
  loadbalancer_id = azurerm_lb.codilime-task-lb.id
  name            = "${var.prefix}-task-probe"
  port            = 80
}

