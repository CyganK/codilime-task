variable "prefix" {
  type = string
  description = "The prefix used for all resources in this example"
  default = "codilime-task"
}

variable "location" {
  type = string
  description = "The Azure location where all resources in this example should be created"
  default = "westeurope"
}

variable "sku" {
  type = string
  description = "The Azure SKU tier"
  default = "Standard"
}

variable "CPU" {
  type = number
  description = "The amaunt of container CPU"
  default = 1
}

variable "memory" {
  type = number
  description = "The amaunt of container memory"
  default = 1.5
}